/* TODO: Maybe use async.js to DRY things up. */
// $(function() {
// 
//   var sel = '.multi-level';
//   // Initialize multi-level menu.
//   /* Raise all sub-menus to the main menu. */
//   $(sel + ' ul ul').each(function() {
//     var diff = $(this).offset().top - $(sel).offset().top;
//     console.assert(diff >= 0, "Weird value");
//     $(this).css('top','-='+diff+'px');
//   });
// 
//   /* Entering sub-menu */
//   $('.multi-level > ul').on('click', 'a', function() {
//     var $ul       = $(this).siblings('ul').first(),
//         ul_height = $ul.outerHeight(true);
// 
//     // If it doesn't have a submenu, return undefined.
//     if ($ul.length == 0)
//       return ;
// 
//     var $menu = $(this).parents('.multi-level').first();
//     console.assert($menu.length != 0,
//                    "No hay punto de referencia para el menu");
// 
//     var width = $menu.css('width');
//     var $root_ul = $menu.find('ul').first();
//     $root_ul.fadeOut(function() {
//       $root_ul.css('left', '-='+width);
//       /* Update min-height of menu */
//       $menu.css('min-height', ul_height);
// 
//       $root_ul.fadeIn();
//     });
//   });
// 
//   /* Coming back from sub-menu */
//   $('.back').click(function() {
//     var $menu      = $(this).parents('.multi-level').first(),
//         $parent_ul = $(this).parents('ul').first()
//                             .parents('ul').first(),
//         height, width;
//     console.assert($menu.length != 0, 
//                    "No hay punto de referencia para el menu");
// 
//     width  = $menu.css('width');
//     height = $parent_ul.outerHeight(),
//     $parent_ul.fadeOut(function() {
//       $menu.find('ul').first().css('left', '+='+width);
//       $menu.css('min-height', height);
//       $parent_ul.fadeIn();
//     })
//   });
// });
